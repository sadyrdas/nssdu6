# Application "Playground"
## Created by
* Dastan Sadyraliyev
## Technologies
* JavaScript
* HTML
* CSS
* Postgresql
## Description
This browser application will allow you to plan the playground of your dreams.
The user can create certain cells and fill them with the necessary parts for his dream playground.
## SetUp
* Clone this repository to your dekstop
* Start this application
* Open js/index.html in your browse
